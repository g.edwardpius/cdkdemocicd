import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { CodePipeline, CodePipelineSource, ShellStep } from 'aws-cdk-lib/pipelines';
// import { PipelineAppStage } from './cdkdemocicd-app-stack';
// import { ManualApprovalStep } from 'aws-cdk-lib/pipelines';
// import { Code } from 'aws-cdk-lib/aws-lambda';


export class CdkdemocicdStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    // AWS CI-CD Pipeline
    const democicdpipeline = new CodePipeline(this,'cdkdemopipeline', {
      synth: new ShellStep('Synth', {
        // Use a connection created using the AWS console to authenticate to GitHub
        // Other sources are available.
        input: CodePipelineSource.connection('Goonasekerampulle Edward Pius/cdkdemocicd', 'main', {
          connectionArn: 'arn:aws:codestar-connections:us-west-2:251933738866:connection/1becfef5-9ef0-440e-aa1f-b6bfb1515cd3'
        }),
        // input: CodePipelineSource.connection('~g.edwardpius/cdkdemocicd', 'main', {
        //   connectionArn: 'arn:aws:codestar-connections:us-west-2:251933738866:connection/78714e67-fc80-4168-86e3-b7db05469069'
        // }),
        // input: CodePipelineSource.gitHub('~g.edwardpius/cdkdemocicd', 'main', {
        //   connectionArn: 'arn:aws:codestar-connections:us-west-2:251933738866:connection/78714e67-fc80-4168-86e3-b7db05469069'
        // }),
        commands: [
          'npm ci',
          'npm run build',
          'npx cdk synth',
        ],
      }),
    });

    // const testingStage = democicdpipeline.addStage(new PipelineAppStage(this, 'test', {
    //   env: { account: '251933738866', region: 'us-west-2' }
    // }));

    // testingStage.addPost(new ManualApprovalStep('approval'));

    // const prodStage = democicdpipeline.addStage(new PipelineAppStage(this, 'prod', {
    //   env: { account: '251933738866', region: 'us-west-2' }
    // }));
  }
}
